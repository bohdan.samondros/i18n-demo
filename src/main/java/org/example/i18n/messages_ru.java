package org.example.i18n;

import java.util.ListResourceBundle;

public class messages_ru extends ListResourceBundle {
    private static final Object[][] contents =
            {
                    {"message.hello", "Хей хей!"},
                    {"specific.message", "bla-bla"}
            };

    public Object[][] getContents() {
        return contents;
    }
}
