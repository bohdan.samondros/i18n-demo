package org.example;

import java.util.Locale;
import java.util.ResourceBundle;

public class ClassResourceBundleDemo {

    public static final Locale LOCALE = new Locale("ru");

    public static void main(String[] args) throws Exception {
        ResourceBundle bundle = ResourceBundle.getBundle("org.example.i18n.messages", LOCALE);

        System.out.println(bundle.getString("message.hello"));
        System.out.println( bundle.getString("specific.message"));
    }
}
