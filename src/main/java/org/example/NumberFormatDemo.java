package org.example;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;

public class NumberFormatDemo {

    public static final Locale LOCALE = new Locale("de", "DE");

    public static void main(String[] args) {
//        example1();
//        example2();
        example3();
    }

    private static void example1() {
        NumberFormat currFmt = NumberFormat.getCurrencyInstance(Locale.GERMANY);
        double amt = 123456.78;
        System.out.println(currFmt.format(amt));
    }

    private static void example3() {
        Currency currency = Currency.getInstance("UAH");
        System.out.println(currency);
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("uk"));
        numberFormat.setCurrency(currency);

        double amt = 123456.78;

        System.out.println(numberFormat.format(amt));
    }

    private static void example2() {
        NumberFormat currFmt = NumberFormat.getCurrencyInstance(LOCALE);
        try {
            Number number = currFmt.parse("123.456,78 €");
            System.out.println(number);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

    }
}
