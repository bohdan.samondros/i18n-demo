package org.example;

import java.util.Arrays;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        Arrays.asList(Locale.getAvailableLocales()).forEach(locale -> {
            try {
                System.out.println("Language Tag: " + locale.toLanguageTag());
                System.out.println("DisplayName: " + locale.getDisplayName());
                System.out.println("Country: " + locale.getCountry());
                System.out.println("ISO3 Country: " + locale.getISO3Country());
                System.out.println("Display Country: " + locale.getDisplayCountry());
                System.out.println("Display Language: " + locale.getLanguage());
                System.out.println("ISO3 Language: " + locale.getISO3Language());
                System.out.println("Display Language: " + locale.getDisplayLanguage(Locale.GERMAN));
                System.out.println("Display Variant: " + locale.getDisplayVariant());
                System.out.println("Display Script: " + locale.getDisplayScript());
                System.out.println("================================================");
            } catch (Exception ex) {
                System.err.println(ex);
            }
        });
    }
}
