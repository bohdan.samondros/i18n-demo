package org.example;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class DateFormattingExample {
    public static final Locale LOCALE = new Locale("de", "DE");

    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)
                .withLocale(LOCALE)
                .withZone(ZoneId.systemDefault());

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime.format(formatter));

        DateTimeFormatter formatter1 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)
                .withLocale(LOCALE)
                .withZone(ZoneId.of("UTC+4"));

        DateTimeFormatter formatter2 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
                .withLocale(LOCALE)
                .withZone(ZoneId.of("UTC+4"));

        DateTimeFormatter formatter3 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                .withLocale(LOCALE)
                .withZone(ZoneId.of("UTC+4"));

        DateTimeFormatter formatter4 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(LOCALE)
                .withZone(ZoneId.of("UTC+4"));
        Instant instant = Instant.now();

        System.out.println(formatter1.format(instant));
        System.out.println(formatter2.format(instant));
        System.out.println(formatter3.format(instant));
        System.out.println(formatter4.format(instant));
    }
}
