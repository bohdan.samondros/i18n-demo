package org.example.messageformat;

import java.text.MessageFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MessageFormatDemo {
    private static final String template = "{2}, {0} уничтожил {1} домов и нанес ущерба на ${3}.";

    public static final Locale LOCALE = Locale.GERMAN;

    public static void main(String[] args) {
        formatCurrentLocale(); // текущая локаль системы
        formatWithLocale(LOCALE); // специфичная локаль
    }

    private static void formatCurrentLocale() {
        String message = MessageFormat.format (template, "ураган",
                99, new GregorianCalendar(1999, 0, 1)
                        .getTime (), 10.0E7);

        System.out.println(message);
    }

    private static void formatWithLocale(Locale locale) {
        MessageFormat format = new MessageFormat(template, locale);
        String message = format.format(new Object[]{"ураган",
                99, new GregorianCalendar(1999, 0, 1)
                        .getTime (), 10.0E7});

        System.out.println(message);
    }
}
