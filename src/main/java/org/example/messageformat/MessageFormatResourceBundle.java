package org.example.messageformat;

import java.text.MessageFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;

public class MessageFormatResourceBundle {
    public static final Locale LOCALE = Locale.US;

    public static void main(String[] args) {
//        formatWithResourceBundle();
//        formatWithTypedParameters(LOCALE);
        choiceDemo(LOCALE);
    }

    private static void formatWithResourceBundle() {
        ResourceBundle bundle = ResourceBundle.getBundle("template", LOCALE);
        MessageFormat format = new MessageFormat(bundle.getString("example"), LOCALE);
        String message = format.format(new Object[]{"ураган",
                99, new GregorianCalendar(1999, 0, 1)
                .getTime(), 10.0E7});

        System.out.println(message);
    }

    private static void formatWithTypedParameters(Locale locale) {
        ResourceBundle bundle = ResourceBundle.getBundle("template", LOCALE);
        MessageFormat format = new MessageFormat(bundle.getString("example.typed"), locale);
        String message = format.format(new Object[]{"ураган",
                99, new GregorianCalendar(1999, 0, 1)
                .getTime (), 10.0E7});

        System.out.println(message);
    }

    private static void choiceDemo(Locale locale) {
        ResourceBundle bundle = ResourceBundle.getBundle("template", LOCALE);
        MessageFormat format = new MessageFormat(bundle.getString("example.3"), locale);
        String message = format.format(new Object[]{"ураган",
                -1, new GregorianCalendar(1999, 0, 1)
                .getTime (), 10.0E7});

        System.out.println(message);
    }
}
