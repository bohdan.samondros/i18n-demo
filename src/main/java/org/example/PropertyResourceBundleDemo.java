package org.example;

import java.util.Locale;
import java.util.ResourceBundle;

public class PropertyResourceBundleDemo {

    public static final Locale LOCALE = new Locale("ru");

    public static void main(String[] args) throws Exception {
        ResourceBundle bundle = ResourceBundle.getBundle("messages", LOCALE);
        ResourceBundle bundleEmail = ResourceBundle.getBundle("email");

        System.out.println(bundle.getString("message.hello"));
        System.out.println( bundle.getString("specific.message"));

        System.out.println( bundle.getString("simple.key"));
    }
}
